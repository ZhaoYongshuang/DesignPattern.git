﻿using System;

namespace Mediator
{
	//伊拉克
	public class Iraq:Country
	{
		public Iraq (UnitedNations mediator):base(mediator)
		{
		}
		//声明
		public void Declare(string message){
			mediator.Declare (message, this);
		}
		//获得消息
		public void GetMessage(string message){
			Console.WriteLine ("伊拉克获得对方信息：" + message);
		}
	}
}

