﻿using System;

namespace FactoryMethod
{
	//加法工厂
	public class AddFactory:IFactory
	{
		public Operation CreateOperation(){
			return new OperationAdd ();
		}
	}
}

