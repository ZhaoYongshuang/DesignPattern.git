﻿using System;

namespace FactoryMethod
{
	//减法类
	public class OperationSub:Operation
	{
		public override double GetResult()
		{
			double result = 0;
			result = NumberA - NumberB;
			return result;
		}
	}
}

