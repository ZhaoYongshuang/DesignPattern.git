﻿using System;

namespace FactoryMethod
{
	//乘法类
	public class MulFactory:IFactory
	{
		public Operation CreateOperation(){
			return new OperationMul();
		}
	}
}

