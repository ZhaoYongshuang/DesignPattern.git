﻿using System;

namespace FactoryMethod
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			IFactory operationFactory = new AddFactory ();
			Operation operation = operationFactory.CreateOperation ();

			operation.NumberA = 1;
			operation.NumberB = 2;

			Console.WriteLine (operation.GetResult ());


			operationFactory = new SubFactory ();
			operation = operationFactory.CreateOperation ();

			operation.NumberA = 1;
			operation.NumberB = 2;

			Console.WriteLine (operation.GetResult ());
		}
	}
}
