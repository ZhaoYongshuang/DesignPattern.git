﻿using System;

namespace FactoryMethod
{
	//加法类
	public class OperationAdd:Operation
	{
		public override double GetResult()
		{
			double result = 0;
			result = NumberA + NumberB;
			return result;
		}
	}
}

