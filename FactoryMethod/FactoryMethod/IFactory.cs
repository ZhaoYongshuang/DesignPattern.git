﻿using System;

namespace FactoryMethod
{
	//工厂接口
	public interface IFactory
	{
		Operation CreateOperation();
	}
}

