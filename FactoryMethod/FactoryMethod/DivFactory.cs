﻿using System;

namespace FactoryMethod
{
	//除法类
	public class DivFactory:IFactory
	{
		public Operation CreateOperation(){
			return new OperationDiv ();
		}
	}
}

