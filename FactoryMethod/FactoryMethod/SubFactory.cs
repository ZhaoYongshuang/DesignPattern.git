﻿using System;

namespace FactoryMethod
{
	//减法类
	public class SubFactory:IFactory
	{
		public Operation CreateOperation(){
			return new OperationSub();
		}
	}
}

