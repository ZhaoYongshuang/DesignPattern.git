﻿using System;

namespace Visitor
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			ObjectStructure objectStructure = new ObjectStructure ();
			objectStructure.Attach (new Man ());
			objectStructure.Attach (new Woman ());

			//成功时的反应
			Success v1=new Success();
			objectStructure.Display (v1);

			//失败时的反应
			Failing v2=new Failing();
			objectStructure.Display (v2);
		}
	}
}
