﻿using System;

namespace Visitor
{
	//状态抽象类
	public abstract class Action
	{
		//得到男人结论或反应
		public abstract void GetManConclusion(Person concreteElementA);

		//得到女人结论或反应
		public abstract void GetWomanConclusion(Person concreteElementA);
	}
}

