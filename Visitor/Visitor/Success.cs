﻿using System;

namespace Visitor
{
	//成功状态类
	public class Success:Action
	{
		public override void GetManConclusion (Person concreteElementA)
		{
			Console.WriteLine ("{0}{1}时，背后多半有一个伟大的女人。", concreteElementA.GetType ().Name, this.GetType ().Name);
		}
		public override void GetWomanConclusion (Person concreteElementA)
		{
			Console.WriteLine ("{0}{1}时，背后大多有一个不成功的男人。", concreteElementA.GetType ().Name, this.GetType ().Name);
		}
	}
}

