﻿using System;

namespace Visitor
{
	//抽象人类
	public abstract class Person
	{
		//接受
		public abstract void Accept(Action visitor);
	}
}

