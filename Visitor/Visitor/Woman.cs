﻿using System;

namespace Visitor
{
	//女人
	public class Woman:Person
	{
		public override void Accept (Action visitor)
		{
			visitor.GetWomanConclusion (this);
		}
	}
}

