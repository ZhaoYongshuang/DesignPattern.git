﻿using System;

namespace Visitor
{
	//男人
	public class Man:Person
	{
		public override void Accept (Action visitor)
		{
			visitor.GetManConclusion (this);
		}
	}
}

