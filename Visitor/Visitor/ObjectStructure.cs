﻿using System;
using System.Collections.Generic;

namespace Visitor
{
	//对象结构
	public class ObjectStructure
	{
		private IList<Person> elements=new List<Person>();
		//增加
		public void Attach(Person element){
			elements.Add (element);
		}
		//移除
		public void Detach(Person element){
			elements.Remove (element);
		}
		//查看显示
		public void Display(Action visitor){
			foreach (Person e in elements) {
				e.Accept (visitor);
			}
		}
	}
}

