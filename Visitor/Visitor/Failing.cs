﻿using System;

namespace Visitor
{
	//失败状态类
	public class Failing:Action
	{
		public override void GetManConclusion (Person concreteElementA)
		{
			Console.WriteLine ("{0}{1}时，闷头喝酒，谁也不用劝。", concreteElementA.GetType ().Name, this.GetType ().Name);
		}
		public override void GetWomanConclusion (Person concreteElementA)
		{
			Console.WriteLine ("{0}{1}时，眼泪汪汪，谁也劝不了。", concreteElementA.GetType ().Name, this.GetType ().Name);
		}
	}
}

