﻿using System;

namespace Prototype
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Resume resume1 = new Resume ("张三");
			resume1.SetPersonalInfo ("男", "23");
			resume1.SetWorkExperience ("1999-2002", "xx公司");

			Resume resume2=(Resume)resume1.Clone();
			resume2.SetWorkExperience ("2004-2005", "YY企业");

			Resume resume3 = (Resume)resume1.Clone ();
			resume3.SetPersonalInfo ("男", "24");

			resume1.Display ();
			resume2.Display ();
			resume3.Display ();
		}
	}
}
