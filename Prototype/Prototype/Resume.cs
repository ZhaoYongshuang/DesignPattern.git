﻿using System;

namespace Prototype
{
	//简历类
	public class Resume:ICloneable
	{
		//姓名
		private string name;
		//性别
		private string sex;
		//年龄
		private string age;
		//工作时间段
		private string timeArea;
		//公司
		private string company;

		public Resume (string name)
		{
			this.name = name;
		}
		//设置个人信息
		public void SetPersonalInfo(string sex,string age){
			this.sex = sex;
			this.age = age;
		}
		//设置工作经历
		public void SetWorkExperience(string timeArea,string company){
			this.timeArea = timeArea;
			this.company = company;
		}
		//显示
		public void Display(){
			Console.WriteLine ("{0} {1} {2}", name, sex, age);
			Console.WriteLine ("工作经历：{0} {1}", timeArea, company);
		}
		public Object Clone(){
			return (Object)this.MemberwiseClone();
		}
	}
}

