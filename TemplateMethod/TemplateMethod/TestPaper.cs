﻿using System;

namespace TemplateMethod
{
	//试题类
	public class TestPaper
	{
		//问题1
		public void TestQuestion1(){
			Console.WriteLine ("问题1");
			Console.WriteLine ("答案：" + Answer1 ());
		}
		//问题2
		public void TestQuestion2(){
			Console.WriteLine ("问题2");
			Console.WriteLine ("答案：" + Answer2 ());
		}
		//问题3
		public void TestQuestion3(){
			Console.WriteLine ("问题3");
			Console.WriteLine ("答案：" + Answer3 ());
		}
		//答案1
		protected virtual string Answer1(){
			return "";
		}
		//答案2
		protected virtual string Answer2(){
			return "";
		}
		//答案3
		protected virtual string Answer3(){
			return "";
		}
	}
}

