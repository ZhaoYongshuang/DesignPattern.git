﻿using System;

namespace Strategy
{
	//打折收费子类
	public class CashRebate:CashSuper
	{
		//打的折扣
		private double moneyRebate=1;

		public CashRebate(string moneyRebate){
			this.moneyRebate=double.Parse(moneyRebate);
		}
		public override double AcceptCash (double money)
		{
			return money*moneyRebate;
		}
	}
}

