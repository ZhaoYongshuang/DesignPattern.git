﻿using System;

namespace Strategy
{
	//现金收取类
	public class CashContext
	{
		//收费方式
		private CashSuper cashSuper;
		public CashContext (CashSuper cashSuper)
		{
			this.cashSuper = cashSuper;
		}
		//获取应付现金结果
		public double GetResult(double money){
			return cashSuper.AcceptCash(money);
		}
	}
}

