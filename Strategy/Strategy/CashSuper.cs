﻿using System;

namespace Strategy
{
	//现金收取抽象类
	public abstract class CashSuper
	{
		//收取的现金
		public abstract double AcceptCash(double money);
	}
}

