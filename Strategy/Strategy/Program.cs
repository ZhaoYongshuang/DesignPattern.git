﻿using System;

namespace Strategy
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//测试 单价200 数量3
			CashContext cashContext = null;
			double totalPrices = 0;

			//正常收费
			cashContext = new CashContext (new CashNormal ());
			totalPrices = cashContext.GetResult (200 * 3);
			Console.WriteLine (totalPrices);

			//满300返100
			cashContext=new CashContext(new CashReturn("300","100"));
			totalPrices = cashContext.GetResult (200 * 3);
			Console.WriteLine (totalPrices);

			//打8折
			cashContext=new CashContext(new CashRebate("0.8"));
			totalPrices = cashContext.GetResult (200 * 3);
			Console.WriteLine (totalPrices);
		}
	}
}
