﻿using System;

namespace Strategy
{
	//正常收费子类
	public class CashNormal:CashSuper{
		public override double AcceptCash(double money){
			return money;
		}
	}
}