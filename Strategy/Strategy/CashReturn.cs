﻿using System;

namespace Strategy
{
	//返利收费子类
	public class CashReturn:CashSuper
	{
		//返现条件
		private double moneyCondition=0;
		//返现的金额
		private double moneyReturn=0;

		public CashReturn (string moneyCondition,string moneyReturn)
		{
			this.moneyCondition = double.Parse (moneyCondition);
			this.moneyReturn = double.Parse (moneyReturn);
		}
		public override double AcceptCash(double money){
			double result=money;
			if (money >= moneyCondition) {
				result = money - Math.Floor (money / moneyCondition) * moneyReturn;		
			}
			return result;
		}
	}
}

