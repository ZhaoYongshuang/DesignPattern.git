﻿using System;

namespace Interpreter
{
	//音符类（TerminalExpression）
	public class Scale:Expression
	{
		string scale="";
		public override void Excute (string key, double value)
		{
			switch (Convert.ToInt32 (value)) {
			case 1:
				scale = "低音";
				break;
			case 2:
				scale = "中音";
				break;
			case 3:
				scale = "高音";
				break;
			}
			Console.Write ("{0} ", scale);
		}
	}
}

