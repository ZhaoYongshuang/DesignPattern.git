﻿using System;

namespace Interpreter
{
	//表达式类（AbstractExpression）
	public abstract class Expression
	{
		//解释器
		public void Interpret(PlayContext context){
			if (context.playText.Length == 0) {
				return;
			} else {
				string playKey = context.playText.Substring (0, 1);
				context.playText = context.playText.Substring (2);
				double playValue = Convert.ToDouble (context.playText.Substring (0, context.playText.IndexOf (" ")));
				context.playText = context.playText.Substring (context.playText.IndexOf (" ") + 1);
				Excute (playKey, playValue);
			}
		}
		//执行
		public abstract void Excute(string key,double value);
	}
}

