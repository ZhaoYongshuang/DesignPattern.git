﻿using System;

namespace Proxy
{
	//送礼物接口
	public interface GiveGift
	{
		//送娃娃
		void GiveDolls();
		//送鲜花
		void GiveFlowers();
		//送巧克力
		void GiveChocolate();
	}
}