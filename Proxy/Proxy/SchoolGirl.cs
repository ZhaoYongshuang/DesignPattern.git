﻿using System;

namespace Proxy
{
	//女孩类
	public class SchoolGirl
	{
		//名字
		private string name;
		public string Name {
			get{ return name; }
			set{ name = value; }
		}
	}
}