﻿using System;

namespace Proxy
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			SchoolGirl schoolGirl = new SchoolGirl ();
			schoolGirl.Name = "李娇娇";

			Proxy proxy = new Proxy (schoolGirl);

			proxy.GiveDolls ();
			proxy.GiveFlowers ();
			proxy.GiveChocolate ();
		}
	}
}