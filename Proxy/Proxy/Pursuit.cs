﻿using System;

namespace Proxy
{
	//追求者类
	public class Pursuit:GiveGift
	{
		//女孩
		SchoolGirl schoolGirl;

		public Pursuit(SchoolGirl schoolGirl){
			this.schoolGirl=schoolGirl;
		}
		//送洋娃娃
		public void GiveDolls(){
			Console.WriteLine(schoolGirl.Name+" 送你洋娃娃");
		}
		//送鲜花
		public void GiveFlowers(){
			Console.WriteLine(schoolGirl.Name+" 送你鲜花");
		}
		//送巧克力
		public void GiveChocolate(){
			Console.WriteLine(schoolGirl.Name+" 送你巧克力");
		}
	}
}