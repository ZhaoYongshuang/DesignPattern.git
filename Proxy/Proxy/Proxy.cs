﻿using System;

namespace Proxy
{
	//代理类
	public class Proxy:GiveGift
	{
		//追求者
		Pursuit pursuit;
		public Proxy(SchoolGirl schoolGirl){
			pursuit = new Pursuit (schoolGirl);
		}
		public void GiveDolls(){
			pursuit.GiveDolls ();
		}
		public void GiveFlowers(){
			pursuit.GiveFlowers ();
		}
		public void GiveChocolate(){
			pursuit.GiveChocolate ();
		}
	}
}