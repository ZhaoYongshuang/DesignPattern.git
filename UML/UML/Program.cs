﻿//动物抽象基类
public abstract class Animal{
	//有生命
	public bool bLiving=true;
	//新陈代谢
	public void Metabolism(Oxygen oxygen,Water water){
	}
	//繁殖
	public void Breeding(){
	}
}
//氧气类
public class Oxygen{	
}
//水类
public class Water{	
}

//鸟类
public class Bird:Animal{
	//翅膀
	private Wing wing;
	public Bird(){
		wing=new Wing();
	}

	//下蛋
	public virtual void LayingEggs(){
		
	}
}
//翅膀
class Wing{
}


//大雁类
class WildGoose:Bird,IFly{
	public override void LayingEggs(){
	}
	public void Fly(){}	
}

//雁群类
class WideGooseAggregate
{
	private WildGoose[] arrayWideGoose;
}

//飞翔接口
interface IFly{
	void Fly();
}

//鸭子类
public class Duck:Bird{
	public override void LayingEggs(){
	}
}
public class DonaldDuck:Duck,ILanguage{
	public void Speak(){
	}
}

//企鹅类
class Penguin:Bird{
	//气候
	private Climate climate;
	public override void LayingEggs(){
	}
}
//讲话接口
interface ILanguage{
	void Speak();
}
//气候类
class Climate{
	public ClimateType climateType;
}
//气候类型
public enum ClimateType{
	HOT,WARM,COOL,COLD
}
