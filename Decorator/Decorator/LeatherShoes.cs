﻿using System;

namespace Decorator
{
	//皮鞋类（ConcreteDecorator）
	public class LeatherShoes:Finery
	{
		public override void Show(){
			Console.Write ("皮鞋");
			base.Show ();
		}
	}
}