﻿using System;

namespace Decorator
{
	//人类（ConcreteComponent)
	public class Person
	{
		//名字
		private string name;

		public Person ()
		{
		}
		public Person(string name){
			this.name = name;
		}
		//展示
		public virtual void Show(){
			Console.WriteLine ("装扮的{0}", name);
		}
	}
}

