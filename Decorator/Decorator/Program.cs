﻿using System;

namespace Decorator
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Person person = new Person ("小三");

			Console.WriteLine ("\n第一种装扮：");

			Sneakers sneaker = new Sneakers ();
			BigTrouser bigTrouser = new BigTrouser ();
			TShirts tShirts = new TShirts ();

			sneaker.Decorate (person);
			bigTrouser.Decorate (sneaker);
			tShirts.Decorate (bigTrouser);
			tShirts.Show ();

			Console.WriteLine ("\n第二种装扮：");

			LeatherShoes leatherShoes = new LeatherShoes ();
			Tie tie = new Tie ();
			Suit suit = new Suit ();

			leatherShoes.Decorate (person);
			tie.Decorate (leatherShoes);
			suit.Decorate (tie);
			suit.Show ();
		}
	}
}
