﻿using System;

namespace Decorator
{
	//服饰类（Decorator）
	public class Finery:Person
	{
		//被装饰的人
		protected Person person;

		//打扮
		public void Decorate(Person person){
			this.person = person;
		}

		public override void Show(){
			if (person != null) {
				person.Show ();
			}
		}
	}
}

