﻿using System;

namespace Singleton
{
	public class SingletonLock
	{
		private static SingletonLock instance;
		//程序运行时创建一个静态只读的进程辅助对象
		private static readonly object syncRoot=new object();
		private SingletonLock ()
		{
		}
		public static SingletonLock GetInstance(){
			//先判断实例是否存在，不存在再加锁处理
			if (instance == null) {
				//在同一个时刻加了锁的那部分程序只有一个线程可以进入
				lock (syncRoot) {
					if (instance == null) {
						instance = new SingletonLock ();
					}
				}
			}
			return instance;
		}
	}
}

