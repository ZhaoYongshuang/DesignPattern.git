﻿using System;

namespace Singleton
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Singleton singleton1 = Singleton.GetInstance ();
			Singleton singleton2 = Singleton.GetInstance ();
			if (singleton1 == singleton2) {
				Console.WriteLine ("俩个对象是相同的实例。");
			}
		}
	}
}
