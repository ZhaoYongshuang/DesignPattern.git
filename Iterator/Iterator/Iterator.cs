﻿using System;

namespace Iterator
{
	//迭代器抽象类
	public abstract class Iterator
	{
		//第一个
		public abstract object First();
		//下一个
		public abstract object Next();
		//是否结束
		public abstract bool IsDone();
		//当前对象
		public abstract object CurrentItem();
	}
}

