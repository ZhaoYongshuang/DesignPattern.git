﻿using System;

namespace Iterator
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			ConcreteAggregate concreteAggregate = new ConcreteAggregate ();

			concreteAggregate [0] = "A";
			concreteAggregate [1] = "B";
			concreteAggregate [2] = "C";
			concreteAggregate [3] = "D";
			concreteAggregate [4] = "E";

			Iterator iterator = new ConcreteIterator (concreteAggregate);
			while (!iterator.IsDone ()) {
				Console.WriteLine (iterator.CurrentItem());
				iterator.Next ();
			}
		}
	}
}
