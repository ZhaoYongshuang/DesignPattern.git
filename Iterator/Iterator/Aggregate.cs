﻿using System;

namespace Iterator
{
	//聚集抽象类
	public abstract class Aggregate
	{
		public abstract Iterator CreateIterator();
	}
}

