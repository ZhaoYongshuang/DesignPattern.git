﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iterator
{
	//具体聚集类
	public class ConcreteAggregate:Aggregate
	{
		private IList<Object> items=new List<Object>();
		public override Iterator CreateIterator ()
		{
			return new ConcreteIterator (this);
		}
		public int Count {
			get{ return items.Count; }
		}
		public object this [int index] {
			get{ return items [index]; }
			set{ items.Insert (index, value); }
		}
	}
}

