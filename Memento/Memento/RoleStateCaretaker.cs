﻿using System;

namespace Memento
{
	//角色状态管理者
	public class RoleStateCaretaker
	{
		private RoleStateMemento memento;
		public RoleStateMemento Memento
		{
			get{ return memento; }
			set{
				memento = value;
			}
		}
	}
}

