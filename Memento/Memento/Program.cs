﻿using System;

namespace Memento
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//大战Boss前
			GameRole gameRole=new GameRole();
			gameRole.GetInitState ();
			gameRole.StateDisplay ();

			//保存进度
			RoleStateCaretaker stateAdmin=new RoleStateCaretaker();
			stateAdmin.Memento = gameRole.SaveState ();

			//大战Boss时，损耗严重
			gameRole.Fight();
			gameRole.StateDisplay ();

			//恢复之前状态
			gameRole.RecoveryState(stateAdmin.Memento);
			gameRole.StateDisplay ();
		}
	}
}
