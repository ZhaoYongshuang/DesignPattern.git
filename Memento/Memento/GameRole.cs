﻿using System;

namespace Memento
{
	//游戏角色类
	public class GameRole
	{
		//生命力
		public int vitality;
		//攻击力
		public int attack;
		//防御力
		public int defense;
		//状态显示
		public void StateDisplay(){
			Console.WriteLine ("角色当前状态：");
			Console.WriteLine ("体力：{0}", this.vitality);
			Console.WriteLine ("攻击力：{0}", this.attack);
			Console.WriteLine ("防御力：{0}", this.defense);
			Console.WriteLine ("");
		}
		//获取初识状态
		public void GetInitState(){
			this.vitality = 100;
			this.attack = 100;
			this.defense = 100;
		}
		//战斗
		public void Fight(){
			this.vitality = 0;
			this.attack = 0;
			this.defense = 0;
		}
		//保存角色状态
		public RoleStateMemento SaveState(){
			return (new RoleStateMemento (vitality, attack, defense));
		}
		//恢复角色状态
		public void RecoveryState(RoleStateMemento memento){
			this.vitality = memento.vitality;
			this.attack = memento.attack;
			this.defense = memento.defense;
		}
	}
}

