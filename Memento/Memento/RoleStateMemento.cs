﻿using System;

namespace Memento
{
	//角色状态存储箱
	public class RoleStateMemento
	{
		//生命力
		public int vitality;
		//攻击力
		public int attack;
		//防御力
		public int defense;
		//状态显示
		public RoleStateMemento(int vitality,int attack,int defense){
			this.vitality=vitality;
			this.attack=attack;
			this.defense=defense;
		}
	}
}

