﻿using System;

namespace AbstractFactory
{
	public class SqlserverUser:IUser
	{
		//插入数据
		public void Insert(User user){
			Console.WriteLine ("在SQL Server中给User表增加一条记录");
		}
		//获取数据
		public User GetUser(int id){
			Console.WriteLine ("在SQL Server中根据ID得到User表一条记录");
			return null;
		}
	}
}

