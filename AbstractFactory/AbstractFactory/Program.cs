﻿using System;

namespace AbstractFactory
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			User user = new User ();
			Department department = new Department ();

//			IFactory factory = new AccessFactory ();
			IFactory factory = new SqlServerFactory ();
		
			IUser iuser = factory.CreateUser ();
			iuser.Insert (user);
			iuser.GetUser (1);

			IDepartment idepartment = factory.CreateDepartment ();
			idepartment.Insert (department);
			idepartment.GetDepartment (1);

		}
	}
}
