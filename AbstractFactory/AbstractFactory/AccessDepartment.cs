﻿using System;

namespace AbstractFactory
{
	public class AccessDepartment:IDepartment
	{
		//插入数据
		public void Insert(Department department){
			Console.WriteLine ("在Access中给Department表增加一条记录");
		}
		//获取数据
		public Department GetDepartment(int id){
			Console.WriteLine ("在Access中根据ID得到Department表一条记录");
			return null;
		}
	}
}

