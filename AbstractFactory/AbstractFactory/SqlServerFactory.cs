﻿using System;

namespace AbstractFactory
{
	public class SqlServerFactory:IFactory
	{
		public IUser CreateUser(){
			return new SqlserverUser();
		}
		public IDepartment CreateDepartment(){
			return new SqlserverDepartment();
		}
	}
}

