﻿using System;

namespace AbstractFactory
{
	//用户类
	public class User
	{
		//ID号
		private int _id;
		public int ID {
			get{ return _id; }
			set{ _id = value; }
		}
		//名字
		private string _name;
		public string Name {
			get{ return _name; }
			set{ _name = value; }
		}
	}
}

