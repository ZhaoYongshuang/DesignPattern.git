﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace Composite
{
	//具体公司类（树枝节点）
	public class ConcreteCompany:Company
	{
		private List<Company> children=new List<Company>();

		public ConcreteCompany (string name):base(name)
		{
		}
		public override void Add (Company company)
		{
			children.Add (company);
		}
		public override void Remove (Company company)
		{
			children.Remove (company);
		}
		public override void Display (int depth)
		{
			Console.WriteLine(new String('-',depth)+name);
			foreach(Company component in children){
				component.Display (depth + 2);
			}
		}
		//履行职责
		public override void LineOfDuty ()
		{
			foreach (Company component in children) {
				component.LineOfDuty ();
			}
		}
	}
}

