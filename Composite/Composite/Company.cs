﻿using System;

namespace Composite
{
	//公司抽象类
	public abstract class Company
	{
		protected string name;
		public Company (string name)
		{
			this.name = name;
		}
		//添加
		public abstract void Add(Company company);
		//移除
		public abstract void Remove(Company company);
		//显示
		public abstract void Display(int depth);
		//履行职责
		public abstract void LineOfDuty();
	}
}

