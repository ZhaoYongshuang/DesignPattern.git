﻿using System;

namespace State
{
	//晚上工作状态
	public class EveningState:State
	{
		public override void WriteProgram(Work work){
			if (work.TaskFinished) {
				work.SetState (new RestState ());
				work.WriteProgram ();
			} else {
				if (work.Hour < 21) {
					Console.WriteLine ("当前时间：{0}点 加班哦，疲惫之极", work.Hour);
				} else {
					work.SetState (new SleepingState ());
					work.WriteProgram ();
				}
			}
		}
	}
}

