﻿using System;

namespace State
{
	//上午工作状态
	public class Forenoon:State
	{
		public override void WriteProgram (Work work)
		{
			if (work.Hour < 12) {
				Console.WriteLine ("当前时间：{0}点 上午工作，精神百倍", work.Hour);
			} else {
				work.SetState (new NoonState ());
				work.WriteProgram ();
			}
		}
	}
}

