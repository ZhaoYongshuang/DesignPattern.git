﻿using System;

namespace State
{
	//睡眠状态
	public class SleepingState:State
	{
		public override void WriteProgram (Work work)
		{
			Console.WriteLine ("当前时间：{0}点不行了，睡着了。", work.Hour);
		}
	}
}

