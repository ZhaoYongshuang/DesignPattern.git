﻿using System;

namespace State
{
	public class Work
	{
		//当前状态
		private State current;
		//时间
		private int hour;
		public int Hour {
			get{ return hour; }
			set{ hour = value; }
		}
		//是否完成
		private bool finish=false;
		public bool TaskFinished {
			get{ return finish; }
			set { finish = value; }
		}

		public Work(){
			current = new Forenoon ();
		}
		//设置状态
		public  void SetState(State s){
			current = s;
		}
		public void WriteProgram(){
			current.WriteProgram (this);
		}
	}
}

