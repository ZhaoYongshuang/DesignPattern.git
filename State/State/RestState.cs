﻿using System;

namespace State
{
	//下班休息状态
	public class RestState:State
	{
		public override void WriteProgram (Work work)
		{
			Console.WriteLine("当前时间：{0}点下班回家了。", work.Hour);
		}
	}
}

