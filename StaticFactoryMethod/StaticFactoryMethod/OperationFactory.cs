﻿using System;

namespace StaticFactoryMethod
{
	public class OperationFactory
	{	
		//根据具体的运算符创建相应的类的实例
		public static Operation CreateOperate(String operate)
		{
			Operation operation=null;
			switch (operate) {
			case "+":
				operation = new OperationAdd ();
				break;
			case "-":
				operation = new OperationSub ();
				break;
			case "*":
				operation = new OperationMul ();
				break;
			case "/":
				operation = new OperationDiv ();
				break;
			}
			return operation;
		}
	}
}