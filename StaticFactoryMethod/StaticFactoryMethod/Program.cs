﻿using System;

namespace StaticFactoryMethod
{
	public class Program
	{
		static void Main(string[] args)
		{
			Operation opertaion;
			opertaion = OperationFactory.CreateOperate ("+");
			opertaion.NumberA = 1;
			opertaion.NumberB = 2;
			double result = opertaion.GetResult ();
			Console.WriteLine (result);
		}
	}
}

