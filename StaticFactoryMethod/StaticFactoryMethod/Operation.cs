﻿using System;

namespace StaticFactoryMethod
{
	public class Operation
	{
		//数值1
		private double numberA=0;
		//数值2
		private double numberB = 0;

		public double NumberA
		{
			get { return numberA; }
			set { numberA = value; }
		}
		public double NumberB
		  {
			get { return numberB; }
			set { numberB = value; }
		}
		//计算结果
		public virtual double GetResult()
		{
			double result = 0;
			return result;
		}
	}
}