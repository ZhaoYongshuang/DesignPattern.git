﻿using System;

namespace Adapter
{
	//球员类
	public abstract class Player
	{
		//名字
		protected string name;

		public Player (string name)
		{
			this.name = name;
		}
		//进攻
		public abstract void Attack();
		//防守
		public abstract void Defense();
	}
}

