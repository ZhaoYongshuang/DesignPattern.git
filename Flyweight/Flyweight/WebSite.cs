﻿using System;

namespace Flyweight
{
	//网站抽象类
	public abstract class WebSite
	{
		public abstract void Use(User user);
	}
}

