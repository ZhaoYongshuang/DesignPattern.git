﻿using System;
using System.Collections;

namespace Flyweight
{
	//网站工厂类
	public class WebSiteFactory
	{
		private Hashtable flyweights=new Hashtable();

		//获得网站分类
		public WebSite GetWebSiteCategory(string key){
			if (!flyweights.ContainsKey (key)) {
				flyweights.Add (key, new ConcreteWebSite (key));
			}
			return((WebSite)flyweights [key]);
		}
		//获得网站分类总数
		public int GetWebSiteCount(){
			return flyweights.Count;
		}
	}
}

