﻿using System;

namespace Flyweight
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			WebSiteFactory webSiteFactory = new WebSiteFactory ();

			WebSite webSite1 = webSiteFactory.GetWebSiteCategory ("产品展示");
			webSite1.Use (new User("张三"));

			WebSite webSite2 = webSiteFactory.GetWebSiteCategory ("产品展示");
			webSite2.Use (new User("李四"));

			WebSite webSite3 = webSiteFactory.GetWebSiteCategory ("产品展示");
			webSite3.Use (new User("王五"));

			WebSite webSite4 = webSiteFactory.GetWebSiteCategory ("博客");
			webSite4.Use (new User("李六"));

			WebSite webSite5 = webSiteFactory.GetWebSiteCategory ("博客");
			webSite5.Use (new User("钱七"));

			WebSite webSite6 = webSiteFactory.GetWebSiteCategory ("博客");
			webSite6.Use (new User("田八"));

			Console.WriteLine ("网站分类总数为{0}", webSiteFactory.GetWebSiteCount ());
		}
	}
}
