﻿using System;

namespace Flyweight
{
	//用户类
	public class User
	{
		//名字
		private string name;

		public User (string name)
		{
			this.name = name;
		}
		public string Name {
			get {
				return name;
			}
		}
	}
}

