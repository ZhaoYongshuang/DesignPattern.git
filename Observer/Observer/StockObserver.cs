﻿using System;

namespace Observer
{
	//看股票的同事
	public class StockObserver:Observer
	{
		public StockObserver (string name,Subject subject):base(name,subject)
		{
		}
		public override void Update(){
			Console.WriteLine("{0} {1} 关闭股票行情，继续工作！",subject.SubjectState,name);
		}
	}
}

