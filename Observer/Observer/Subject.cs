﻿using System;

namespace Observer
{
	//抽象通知者接口
	public interface Subject
	{
		//添加
		void Attach(Observer observer);
		//减少
		void Detach(Observer observer);
		//通知
		void Notify();
		//主题状态
		string SubjectState{
			get;
			set;
		}
	}
}

