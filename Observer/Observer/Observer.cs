﻿using System;

namespace Observer
{
	//抽象观察者
	public abstract class Observer
	{
		//观察者名字
		protected string name;
		//主题
		protected Subject subject;

		public Observer (string name,Subject subject)
		{
			this.name = name;
			this.subject = subject;
		}
		//更新
		public abstract void Update();
	}
}

