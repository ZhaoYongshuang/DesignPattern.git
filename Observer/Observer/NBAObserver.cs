﻿using System;

namespace Observer
{
	public class NBAObserver:Observer
	{
		public NBAObserver (string name,Subject subject):base(name,subject)
		{
		}
		public override void Update(){
			Console.WriteLine("{0} {1} 关闭股票行情，继续工作！",subject.SubjectState,name);
		}
	}
}

