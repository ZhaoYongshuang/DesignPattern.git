﻿using System;

namespace Observer
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//老板王五
			Boss boss = new Boss ();
			//看股票的同事
			StockObserver stockObserver = new StockObserver ("张三", boss);
			//看NBA的同事
			NBAObserver nbaObserver = new NBAObserver ("李四", boss);

			boss.Attach (stockObserver);
			boss.Attach (nbaObserver);

			boss.Detach (stockObserver);
			//老板回来
			boss.SubjectState = "王五回来了!";
			//发布通知
			boss.Notify ();
		}
	}
}
