﻿using System;

namespace Bridge
{
	//手机品牌抽象类
	public abstract class HandsetBrand
	{
		protected HandsetSoft soft;

		//设计手机软件
		public void SetHandsetSoft(HandsetSoft soft){
			this.soft=soft;
		}
		//运行
		public abstract void Run();
	}
}

