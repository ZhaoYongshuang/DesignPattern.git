﻿using System;

namespace Bridge
{
	//手机软件抽象类
	public abstract class HandsetSoft
	{
		public abstract void Run();
	}
}

