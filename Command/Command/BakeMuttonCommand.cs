﻿using System;

namespace Command
{
	//烤羊肉串命令
	public class BakeMuttonCommand:Command
	{
		public BakeMuttonCommand (Barbecuer receiver):base(receiver)
		{
		}
		public override void ExcuteCommand ()
		{
			receiver.BakeMutton ();
		}
	}
}

