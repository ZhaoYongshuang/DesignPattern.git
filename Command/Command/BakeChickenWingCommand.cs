﻿using System;

namespace Command
{
	//烤鸡翅命令
	public class BakeChickenWingCommand:Command
	{
		public BakeChickenWingCommand (Barbecuer receiver):base(receiver)
		{
		}
		public override void ExcuteCommand ()
		{
			receiver.BakeChickenWing ();
		}
	}
}

