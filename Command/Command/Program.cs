﻿using System;

namespace Command
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//开店前的准备
			Barbecuer barbecuer=new Barbecuer();
			Command bakeMuttonCommand = new BakeMuttonCommand (barbecuer);
			Command bakeChickenWingCommand = new BakeChickenWingCommand (barbecuer);

			Waiter waiter = new Waiter ();

			//开店营业
			waiter.SetOrder(bakeMuttonCommand);
			waiter.SetOrder (bakeChickenWingCommand);
			waiter.Notify ();
		}
	}
}
