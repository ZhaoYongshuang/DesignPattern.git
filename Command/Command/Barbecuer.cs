﻿using System;

namespace Command
{
	//烤肉串者
	public class Barbecuer
	{
		//烤羊肉
		public void BakeMutton ()
		{
			Console.WriteLine ("烤羊肉串！");
		}
		//烤鸡翅
		public void BakeChickenWing(){
			Console.WriteLine("烤鸡翅!");
		}
	}
}

