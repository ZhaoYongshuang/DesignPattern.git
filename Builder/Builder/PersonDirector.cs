﻿using System;

namespace Builder
{
	//造人指挥者
	public class PersonDirector
	{
		private PersonBuilder personBuilder;

		public PersonDirector (PersonBuilder personBuilder)
		{
			this.personBuilder = personBuilder;
		}
		public void CreatePerson(){
			personBuilder.BuildHead();
			personBuilder.BuildBody ();
			personBuilder.BuildArmLeft ();
			personBuilder.BuildArmRight ();
			personBuilder.BuildLegLeft ();
			personBuilder.BuildLegRight ();
		}
	}
}

