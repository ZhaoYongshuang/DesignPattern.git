﻿using System;

namespace Builder
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Pen p = new Pen (Color.Yellow);
			PersonThinBuilder personThinBuilder = new PersonThinBuilder (pictureBox1.CreateGraphics (), p);
			PersonDirector personDirectorThin=new PersonDirector(PersonThinBuilder);
			personDirectorThin.CreatePerson ();

			PersonFatBuilder personFatBuilder = new PersonFatBuilder (pictureBox1.CreateGraphics (), p);
			PersonDirector personDirectorFat=new PersonDirector(personFatBuilder);
			personDirectorFat.CreatePerson ();
		}
	}
}
