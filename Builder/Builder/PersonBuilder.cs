﻿using System;

namespace Builder
{
	//造人抽象类
	public abstract class PersonBuilder
	{
		//画布类
		protected Graphics g;
		//画笔
		protected Pen p;

		public PersonBuilder(Graphics g,Pen p){
			this.g=g;
			this.p = p;
		}
		//头
		public abstract void BuildHead();
		//身体
		public abstract void BuildBody();
		//左胳膊
		public abstract void BuildArmLeft();
		//右胳膊
		public abstract void BuildArmRight();
		//左腿
		public abstract void BuildLegLeft();
		//右腿
		public abstract void BuildLegRight();
	}
}

