﻿using System;

namespace Facade
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Fund fund = new Fund ();
			//基金购买
			fund.BuyFund();
			//基金赎回
			fund.SellFund();
		}
	}
}
