﻿using System;

namespace Facade
{
	//股票3
	public class Stock3
	{
		//买股票
		public void Sell(){
			Console.WriteLine ("股票3卖出");
		}
		//买股票
		public void Buy(){
			Console.WriteLine ("股票3买入");
		}
	}
}

