﻿using System;

namespace Facade
{
	//股票1
	public class Stock1
	{
		//买股票
		public void Sell(){
			Console.WriteLine ("股票1卖出");
		}
		//买股票
		public void Buy(){
			Console.WriteLine ("股票1买入");
		}
	}
}

