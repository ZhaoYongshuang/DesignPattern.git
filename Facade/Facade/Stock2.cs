﻿using System;

namespace Facade
{
	//股票2
	public class Stock2
	{
		//买股票
		public void Sell(){
			Console.WriteLine ("股票2卖出");
		}
		//买股票
		public void Buy(){
			Console.WriteLine ("股票2买入");
		}
	}
}

