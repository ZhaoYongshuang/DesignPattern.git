﻿using System;

namespace Facade
{
	//国债1
	public class NationalDebt1
	{
		//买国债
		public void Sell(){
			Console.WriteLine ("国债1卖出");
		}
		//买国债
		public void Buy(){
			Console.WriteLine ("国债1买入");
		}
	}
}

