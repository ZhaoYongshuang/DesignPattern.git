﻿using System;

namespace Facade
{
	//房地产1
	public class Realty1
	{
		//买房地产
		public void Sell(){
			Console.WriteLine ("房地产1卖出");
		}
		//买房地产
		public void Buy(){
			Console.WriteLine ("房地产1买入");
		}
	}
}

