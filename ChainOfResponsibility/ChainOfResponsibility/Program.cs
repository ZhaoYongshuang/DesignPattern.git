﻿using System;

namespace ChainOfResponsibility
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			CommonManager commonManager = new CommonManager ("经理");
			Majordomo majordomo = new Majordomo ("总监");
			GeneralManager generalManager = new GeneralManager ("总经理");

			commonManager.SetSuperior (majordomo);
			majordomo.SetSuperior (generalManager);

			Request request = new Request ();
			request.requestType = "请假";
			request.requestContent = "XX请假";
			request.number = 1;
			commonManager.RequestApplications (request);

			Request request2 = new Request ();
			request2.requestType = "请假";
			request2.requestContent = "XX请假";
			request2.number = 4;
			commonManager.RequestApplications (request2);

			Request request3 = new Request ();
			request3.requestType = "加薪";
			request3.requestContent = "XX请求加薪";
			request3.number = 500;
			commonManager.RequestApplications (request3);

			Request request4 = new Request ();
			request4.requestType = "加薪";
			request4.requestContent = "XX请求加薪";
			request4.number = 1000;
			commonManager.RequestApplications (request4);
		}
	}
}
