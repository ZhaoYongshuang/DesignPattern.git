﻿using System;

namespace ChainOfResponsibility
{
	//经理类
	public class CommonManager:Manager
	{
		public CommonManager(string name):base(name){}
		public override void RequestApplications (Request request)
		{
			if (request.requestType == "请假" && request.number <= 2) {
				Console.WriteLine ("{0}:{1}数量{2}被批准", name, request.requestContent, request.number);
			} else{
				if (superior != null) {
					superior.RequestApplications (request);
				}
			}
		}
	}
}

