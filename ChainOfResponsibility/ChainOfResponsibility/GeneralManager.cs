﻿using System;

namespace ChainOfResponsibility
{
	//总经理类
	public class GeneralManager:Manager
	{
		public GeneralManager (string name):base(name)
		{
		}
		public override void RequestApplications (Request request)
		{
			if (request.requestType == "请假") {
				Console.WriteLine ("{0}:{1}数量{2}被批准", name, request.requestContent, request.number);
			} else if (request.requestType == "加薪" && request.number <= 500) {
				Console.WriteLine ("{0}:{1}数量{2}被批准", name, request.requestContent, request.number);
			} else if (request.requestType == "加薪" && request.number > 500) {
				Console.WriteLine ("{0}:{1}数量{2}再说吧", name, request.requestContent, request.number);
			}
		}
	}
}

