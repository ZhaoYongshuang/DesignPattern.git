﻿using System;

namespace ChainOfResponsibility
{
	//总监类
	public class Majordomo:Manager
	{
		public Majordomo (string name):base(name)
		{
		}
		public override void RequestApplications (Request request)
		{
			if (request.requestType == "请假" && request.number <= 5) {
				Console.WriteLine ("{0}:{1}数量{2}被批准", name, request.requestContent, request.number);
			} else{
				if (superior != null) {
					superior.RequestApplications (request);
				}
			}
		}
	}
}

