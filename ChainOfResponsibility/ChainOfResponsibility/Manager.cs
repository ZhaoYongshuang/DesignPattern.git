﻿using System;

namespace ChainOfResponsibility
{
	//管理者
	public abstract class Manager
	{
		protected string name;
		//管理者的上级
		protected Manager superior;

		public Manager (string name)
		{
			this.name = name;
		}
		//设置管理者的上级
		public void SetSuperior(Manager superior){
			this.superior = superior;
		}
		//申请请求
		public abstract void RequestApplications(Request request);
	}
}

