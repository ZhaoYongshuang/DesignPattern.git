﻿using System;

namespace ObserverDelegate
{
	public interface Subject
	{
		void Notify();
		string SubjectState{
			get;
			set;
		}
	}
}

