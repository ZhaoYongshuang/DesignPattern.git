﻿using System;

namespace ObserverDelegate
{
	public class Boss:Subject
	{
		public string action;

		public delegate void EventHandler();
		//声明一事件Update，类型为委托EventHandler
		public event EventHandler Update;
		public void Notify(){
			if (Update != null) {
				Update ();
			}
		}
		public string SubjectState {
			get{ return action; }
			set{ action = value; }
		}
	}
}

