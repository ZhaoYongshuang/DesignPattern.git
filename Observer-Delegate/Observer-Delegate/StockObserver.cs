﻿using System;

namespace ObserverDelegate
{
	public class StockObserver
	{
		private string name;
		private Subject subject;
		public StockObserver(string name,Subject subject){
			this.name = name;
			this.subject = subject;
		}
		//关闭股票行情
		public void CloseStockMarket ()
		{
			Console.WriteLine ("{0} {1} 关闭股票行情，继续工作！", subject.SubjectState, name);
		}
	}
}

