﻿using System;

namespace ObserverDelegate
{
	public class NBAObserver
	{
		private string name;
		private Subject subject;
		public NBAObserver(string name,Subject subject){
			this.name = name;
			this.subject = subject;
		}
		//关闭股票行情
		public void CloseNBADirectSeeding ()
		{
			Console.WriteLine ("{0} {1} 关闭NBA直播，继续工作！", subject.SubjectState, name);
		}
	}
}

