﻿using System;

namespace ObserverDelegate
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//Boss
			Boss boss=new Boss();
			//看股票的同时
			StockObserver stockObserver=new StockObserver("张三",boss);
			//看NBA的同时
			NBAObserver nbaObserver = new NBAObserver ("李四", boss);
			boss.Update += stockObserver.CloseStockMarket;
			boss.Update += nbaObserver.CloseNBADirectSeeding;
			//boss 回来
			boss.SubjectState="boos 回来了！";
			//发出通知
			boss.Notify();
		}
	}
}
